﻿using UnityEngine;

public class ControladorCamara : MonoBehaviour
{
    public GameObject Jugador;
    private Vector3 PosicionRelativa;

    public void Start()
    {
        PosicionRelativa = transform.position - Jugador.transform.position;        
    }

    public void LateUpdate()
    {
        transform.position = Jugador.transform.position + PosicionRelativa;
    }
}
