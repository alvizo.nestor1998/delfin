﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorJugador : MonoBehaviour
{
    Rigidbody Rb;
    public void Awake()
    {
        Rb = GetComponent<Rigidbody>();
    }
    public void FixedUpdate() 
    {
        float MovimientoHorizontal = Input.GetAxis("Horizontal");
        float MovimientoVertical = Input.GetAxis("Vertical");
        Vector3 Movimiento = new Vector3(MovimientoHorizontal,0,MovimientoVertical);
        Rb.AddForce(Movimiento * 21 );
    }

}
