﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgentScript : MonoBehaviour
{
    public float speed;

    private Rigidbody rb;

    private GameObject objective;

    void Start ()
    {
        rb = GetComponent<Rigidbody>();
        GameObject[] objectives = GameObject.FindGameObjectsWithTag("Objective");

        int size = objectives.Length;
        int index = Random.Range(0, size);

        Debug.Log(index);
        objective = objectives[index];
    }

    void FixedUpdate ()
    {

        Vector3 centerPoint = objective.transform.position;
        Vector3 desired;
        Vector3 movement = centerPoint - rb.transform.position;
        movement.Normalize();

        if (centerPoint.magnitude <= 10)
        {
            desired = speed * movement * (centerPoint.magnitude / 10 );
        }
        else
        {
            desired = movement * speed;
        }
        rb.AddForce(desired);

        
    }

   
}


    /* If (velocity + steering) equals zero, then there is no movement
    velocity = truncate(velocity + steering, max_speed);
    position = position + velocity ;
 
    public void truncate(vector, Vector3D, max;Number)
    {
        var i ;Number
        i = max / vector.length;
        i = i < 1.0 si i : 1.0;
        vector.scaleBy(i);
    }
}
*/