﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Huida : MonoBehaviour
{
    public float speed;

    private Rigidbody rb;

    private GameObject objective;

    private GameObject objective2;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        GameObject[] objectives = GameObject.FindGameObjectsWithTag("Agent");
        GameObject[] objectives2 = GameObject.FindGameObjectsWithTag("Obstaculo");

        int size = objectives.Length;
        int index = Random.Range(0, size);
        int size2 = objectives2.Length;
        int index2 = Random.Range(0, size2);


        Debug.Log(index);
        objective = objectives[index];
        objective2 = objectives2[index2];
    }

    void FixedUpdate()
    {

        Vector3 centerPoint = objective.transform.position - rb.transform.position;
        Vector3 centerPoint2 = objective2.transform.position - rb.transform.position;

        Vector3 movement = -centerPoint;// +rb.transform.position;
        movement.Normalize();

        Vector3 movement2 = -centerPoint2;// +rb.transform.position;
        movement2.Normalize();

        Vector3 desired;
        //Condicion para acelerar o desacelerar cuando lo persigan
        if (centerPoint.magnitude <=5)
        {
            desired = speed * movement *(5/centerPoint.magnitude);
        }
        else 
        {
            desired = movement * speed;
        }

          rb.AddForce(desired);
        
     }        
  }

